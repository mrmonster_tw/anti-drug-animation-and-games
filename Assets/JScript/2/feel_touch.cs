﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class feel_touch : MonoBehaviour
{
    public GameObject man;
    public GameObject feel;
    public Text middle_word;
    int check = 1;
    bool retu;
    public GameObject page_1;
    public GameObject page_2;
    public GameObject quit;


    public Texture man_1;
    public Texture man_2;
    public Texture man_3;
    public Texture man_4;
    public Texture man_5;
    public Texture feel_1;
    public Texture feel_2;
    public Texture feel_3;
    public Texture feel_4;
    public Texture feel_5;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.GetComponent<AudioSource>().Play() ;
        }
        if (Input.GetMouseButtonUp(0) && check < 6 && retu == false)
        {
            check++;
            match_fn(check);
        }
        if (check == 1)
        {
            retu = false;
        }
    }
    public void back_fn()
    {
        retu = true;
        if (retu && check > 1)
        {
            check--;
            match_fn(check);
            StartCoroutine(hold_wait());
        }
    }
    void match_fn(int xx)
    {
        if (xx == 1)
        {
            man.GetComponent<RawImage>().texture = man_1;
            feel.GetComponent<RawImage>().texture = feel_1;
            middle_word.text = "騎機車時遇到滂沱大雨";
            page_1.SetActive(true);
            page_2.SetActive(false);
            quit.SetActive(false);
        }
        if (xx == 2)
        {
            man.GetComponent<RawImage>().texture = man_2;
            feel.GetComponent<RawImage>().texture = feel_2;
            middle_word.text = "期末考快逼近還在打電動";
            page_1.SetActive(true);
            page_2.SetActive(false);
            quit.SetActive(false);
        }
        if (xx == 3)
        {
            man.GetComponent<RawImage>().texture = man_3;
            feel.GetComponent<RawImage>().texture = feel_3;
            middle_word.text = "和好友去唱歌付帳時才發現錢包沒錢";
            page_1.SetActive(true);
            page_2.SetActive(false);
            quit.SetActive(false);
        }
        if (xx == 4)
        {
            man.GetComponent<RawImage>().texture = man_4;
            feel.GetComponent<RawImage>().texture = feel_4;
            middle_word.text = "努力打工期待新手機，才發現價格昂貴買不起";
            page_1.SetActive(true);
            page_2.SetActive(false);
            quit.SetActive(false);
        }
        if (xx == 5)
        {
            man.GetComponent<RawImage>().texture = man_5;
            feel.GetComponent<RawImage>().texture = feel_5;
            middle_word.text = "努力唸書結果還被當掉";
            page_1.SetActive(true);
            page_2.SetActive(false);
            quit.SetActive(false);
        }
        if (xx == 6)
        {
            quit.SetActive(true);
            page_2.SetActive(true);
            page_1.SetActive(false);
        }
    }
    private IEnumerator hold_wait()
    {
        yield return new WaitForSeconds(0.1f);
        retu = false;
    }
    public void quit_fn()
    {
        Application.Quit();
    }
}
