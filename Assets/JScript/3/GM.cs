﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GM : MonoBehaviour
{
    public GameObject block_1;
    public GameObject block_2;
    public GameObject block_3;
    public GameObject block_4;
    public GameObject block_5;
    public GameObject block_6;
    public GameObject block_7;
    public GameObject block_8;
    public GameObject block_9;
    public GameObject block_10;
    public GameObject block_11;
    public GameObject block_12;
    public GameObject block_13;
    public GameObject block_14;

    public bool mousedown;
    public float time;
    public Text time_tx;
    public Text time_tx_1;
    public int matchtime;

    public GameObject score;
    public GameObject restart3;
    public GameObject quit3;

    // Start is called before the first frame update
    void Start()
    {
        block_1.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_2.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_3.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_4.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_5.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_6.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_7.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_8.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_9.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_10.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_11.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_12.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_13.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
        block_14.transform.position = new Vector3(Random.RandomRange(-9, 9), Random.RandomRange(-6, 6), 0);
    }

    // Update is called once per frame
    void Update()
    {
        block_1.transform.localEulerAngles = new Vector3(0,0,0);
        block_2.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_3.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_4.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_5.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_6.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_7.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_8.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_9.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_10.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_11.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_12.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_13.transform.localEulerAngles = new Vector3(0, 0, 0);
        block_14.transform.localEulerAngles = new Vector3(0, 0, 0);


        if (matchtime < 14)
        {
            time = time + 1f * Time.deltaTime;
            time_tx.text = time.ToString("0.0");
        }
        if (matchtime == 14)
        {
            time_tx.enabled = false;
            time_tx_1.text = time_tx.text;
            score.SetActive(true);
            StartCoroutine(show_score());
        }
    }
    private IEnumerator show_score()
    {
        yield return new WaitForSeconds(1.5f);
        time_tx_1.enabled = true;
        restart3.SetActive(true);
        quit3.SetActive(true);
    }
    public void restart_3()
    {
        this.GetComponent<AudioSource>().Play();
        SceneManager.LoadScene(0);
    }
    public void quit_3()
    {
        this.GetComponent<AudioSource>().Play();
        Application.Quit();
    }
}
