﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dragmatch : MonoBehaviour
{
    public Vector3 offset;
    public GameObject gm;
    public bool hold;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) && hold)
        {
            gm.GetComponent<AudioSource>().Play();
            this.gameObject.SetActive(false);
            gm.GetComponent<GM>().matchtime++;
        }
    }
    void OnMouseDown()
    {
        print("aa");
        this.GetComponent<BoxCollider2D>().isTrigger = true;
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
        this.GetComponent<SpriteRenderer>().sortingOrder = 2;
    }
    void OnMouseDrag()
    {
        print("bb");
        Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f);
        transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;
    }
    void OnMouseUp()
    {
        this.GetComponent<BoxCollider2D>().isTrigger = false;
        this.GetComponent<SpriteRenderer>().sortingOrder = 0;
    }
    void OnTriggerStay2D(Collider2D col)
    {
        math_fn(col,"Q1","A1");
        math_fn(col, "Q2", "A2");
        math_fn(col, "Q3", "A3");
        math_fn(col, "Q4", "A4");
        math_fn(col, "Q5", "A5");
        math_fn(col, "Q6", "A6");
        math_fn(col, "Q7", "A7");
    }
    void OnTriggerExit2D(Collider2D col)
    {
        hold = false;
    }
    void math_fn(Collider2D rr , string xx , string yy)
    {
        if (this.name == xx && rr.name == yy)
        {
            hold = true;
        }
        if (this.name == yy && rr.name == xx)
        {
            hold = true;
        }
    }
}
