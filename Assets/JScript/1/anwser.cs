﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anwser : MonoBehaviour
{
	public bool anwsered;
	public int score;
	public string right_ans;

	public Animator[] anis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (anwsered)
		{
			anis = GetComponentsInChildren<Animator> ();
			StartCoroutine (wait_close ());
		}
    }
	private IEnumerator wait_close () 
	{
		yield return new WaitForSeconds (1f);
		anis [0].SetBool ("done",true);
		anis [1].SetBool ("done",true);
		anis [2].SetBool ("done",true);
		anis [3].SetBool ("done",true);
		yield return new WaitForSeconds (1f);
		anis [0].enabled = false;
		anis [1].enabled = false;
		anis [2].enabled = false;
		anis [3].enabled = false;
	}
}
