﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TalkDone : MonoBehaviour
{
	public GameObject talk_bg;
    public GameObject talk_1_ob;
    public GameObject talk_2_ob;
    public GameObject options_1;
    public GameObject answer_1;

    public GameObject talk_3_ob;
    public GameObject options_2;
    public GameObject answer_2;

	public GameObject talk_4_ob;
	public GameObject options_3;
	public GameObject answer_3;

	public GameObject talk_5_ob;
	public GameObject options_4;
	public GameObject answer_4;

	public GameObject talk_6_ob;
	public GameObject options_5;
	public GameObject answer_5;

	public GameObject talk_7_ob;
	public GameObject quit;

    public GameObject yes;
    public GameObject no;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	void talk_1_bt()
	{
        talk_1_ob.SetActive(false);
        talk_2_ob.SetActive(true);
        this.GetComponent<Animator>().Play("change_1");
    }
	void opt_1_bt()
    {
        options_1.SetActive(true);
    }
    public void answer_1_fn(string xx)
    {
        if (xx == "yes")
        {
            yes.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        if (xx == "no")
        {
            no.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        this.GetComponent<AudioSource>().Play();
        answer_1.SetActive(true);
        talk_2_ob.SetActive(false);
        options_1.SetActive(false);
    }
	void talk_3_bt()
	{
		answer_1.SetActive(false);
		talk_2_ob.SetActive(false);
		talk_3_ob.SetActive(true);
		this.GetComponent<Animator>().Play("change_2");
	}
	void opt_2_bt()
	{
		options_2.SetActive(true);
	}
	public void answer_2_fn(string xx)
	{
        if (xx == "yes")
        {
            yes.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        if (xx == "no")
        {
            no.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        this.GetComponent<AudioSource>().Play();
        answer_2.SetActive(true);
		talk_3_ob.SetActive(false);
		options_2.SetActive(false);
	}
	void talk_4_bt()
	{
		answer_2.SetActive(false);
		talk_4_ob.SetActive(true);
		this.GetComponent<Animator>().Play("change_3");
	}
	void opt_3_bt()
	{
		options_3.SetActive(true);
	}
	public void answer_3_fn(string xx)
	{
        if (xx == "yes")
        {
            yes.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        if (xx == "no")
        {
            no.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        this.GetComponent<AudioSource>().Play();
        answer_3.SetActive(true);
		talk_4_ob.SetActive(false);
		options_3.SetActive(false);
	}
	void talk_5_bt()
	{
		answer_3.SetActive(false);
		talk_5_ob.SetActive(true);
		this.GetComponent<Animator>().Play("change_4");
	}
	void opt_4_bt()
	{
		options_4.SetActive(true);
	}
	public void answer_4_fn(string xx)
	{
        if (xx == "yes")
        {
            yes.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        if (xx == "no")
        {
            no.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        this.GetComponent<AudioSource>().Play();
        answer_4.SetActive(true);
		talk_5_ob.SetActive(false);
		options_4.SetActive(false);
	}
	void talk_6_bt()
	{
		answer_4.SetActive(false);
		talk_6_ob.SetActive(true);
		this.GetComponent<Animator>().Play("change_5");
	}
	void opt_5_bt()
	{
		options_5.SetActive(true);
	}
	public void answer_5_fn(string xx)
	{
        if (xx == "yes")
        {
            yes.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        if (xx == "no")
        {
            no.SetActive(true);
            StartCoroutine(closeyn_fn());
        }
        this.GetComponent<AudioSource>().Play();
        answer_5.SetActive(true);
		talk_6_ob.SetActive(false);
		options_5.SetActive(false);
	}
	void rotate_fn()
	{
		answer_5.SetActive(false);
		talk_bg.SetActive(false);
		this.GetComponent<Animator>().Play("change_6");
		StartCoroutine (hold_wait());
	}
	private IEnumerator hold_wait () 
	{
		yield return new WaitForSeconds (3f);
		talk_bg.SetActive(true);
		talk_7_ob.SetActive(true);
		yield return new WaitForSeconds (3f);
		quit.SetActive(true);
	}
	public void quit_bt()
	{
        this.GetComponent<AudioSource>().Play();
        Application.Quit();
	}
    private IEnumerator closeyn_fn()
    {
        yield return new WaitForSeconds(1f);
        yes.SetActive(false);
        no.SetActive(false);
    }
}
