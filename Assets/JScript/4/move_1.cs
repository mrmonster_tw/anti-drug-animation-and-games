﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_1 : MonoBehaviour
{
    Vector3 offset;
    public GameObject gm_4sc;
    public GameObject arrow;
    public int check;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.localPosition.y <= -2.8 && check == 0)
        {
            this.GetComponent<AudioSource>().Play();
            gm_4sc.GetComponent<GM_4>().ranques_fn();
            arrow.SetActive(false);
            StartCoroutine(hold_fn());
            check = 1;
        }
    }
    private IEnumerator hold_fn()
    {
        yield return new WaitForSeconds(0.2f);
        this.gameObject.SetActive(false);
    }
    void OnMouseDown()
    {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(-3.98f, Input.mousePosition.y, 10.0f));
        this.GetComponent<SpriteRenderer>().sortingOrder = 2;
    }
    void OnMouseDrag()
    {
        if (this.transform.localPosition.y <= 0.04)
        {
            Vector3 newPosition = new Vector3(-3.98f, Input.mousePosition.y, 10.0f);
            transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;
        }
    }
    void OnMouseUp()
    {
        if (this.transform.localPosition.y > -2.8)
        {
            this.transform.localPosition = new Vector3(-3.98f, 0.03f, 0);
            this.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
    }
}
