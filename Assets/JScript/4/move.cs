﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    Vector3 offset;
    public bool chose;
    public GameObject gm_4;
    public GameObject arrow;
    public GameObject paper_white;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) && chose)
        {
            arrow.SetActive(true);
            paper_white.SetActive(true);
            paper_white.transform.position = new Vector3(-3.98f,0.03f,0);
            paper_white.GetComponent<move_1>().check = 0;
            this.GetComponent<Animator>().enabled = true;
            Destroy(this.gameObject,0.2f);
        }
    }
    void OnMouseDown()
    {
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
        this.GetComponent<SpriteRenderer>().sortingOrder = 2;
    }
    void OnMouseDrag()
    {
        Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f);
        transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;
    }
    void OnMouseUp()
    {
        if (chose == false)
        {
            this.transform.localPosition = new Vector3(-3.98f, -2.8f, 0);
            this.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
    }
}
