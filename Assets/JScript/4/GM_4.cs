﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GM_4 : MonoBehaviour
{
    public GameObject block_1;
    public GameObject block_2;
    public GameObject block_3;
    public GameObject block_4;
    public GameObject block_5;
    public GameObject block_6;
    public GameObject block_7;
    public GameObject block_8;
    public GameObject block_9;
    public GameObject block_10;

    public int block_1_sc;
    public int block_2_sc;
    public int block_3_sc;
    public int block_4_sc;
    public int block_5_sc;
    public int block_6_sc;
    public int block_7_sc;
    public int block_8_sc;
    public int block_9_sc;
    public int block_10_sc;

    public GameObject block_bar_1;
    public GameObject block_bar_2;
    public GameObject block_bar_3;
    public GameObject block_bar_4;
    public GameObject block_bar_5;
    public GameObject block_bar_6;
    public GameObject block_bar_7;
    public GameObject block_bar_8;
    public GameObject block_bar_9;
    public GameObject block_bar_10;
    float bar_nu_1;
    float bar_nu_2;
    float bar_nu_3;
    float bar_nu_4;
    float bar_nu_5;
    float bar_nu_6;
    float bar_nu_7;
    float bar_nu_8;
    float bar_nu_9;
    float bar_nu_10;
    float bar_nu_11;
    float bar_nu_12;
    float bar_nu_13;
    float bar_nu_14;
    float bar_nu_15;
    float bar_nu_16;
    float bar_nu_17;
    float bar_nu_18;
    float bar_nu_19;
    float bar_nu_20;

    public int count = 21;
    public GameObject[] quesions;
    public GameObject bg;
    public GameObject paper_white_1;
    public GameObject arrow_1;

    public bool go;
    public GameObject restart;
    public GameObject quit;

    public int kindcheck = 0;
    int once = 0;
    public GameObject vic;
    public Texture v1;
    public Texture v2;
    public Texture v3;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        quesions = GameObject.FindGameObjectsWithTag("paper_head");

        if (go)
        {
            ranques_fn(block_1_sc, block_bar_1);
            ranques_fn(block_2_sc, block_bar_2);
            ranques_fn(block_3_sc, block_bar_3);
            ranques_fn(block_4_sc, block_bar_4);
            ranques_fn(block_5_sc, block_bar_5);
            ranques_fn(block_6_sc, block_bar_6);
            ranques_fn(block_7_sc, block_bar_7);
            ranques_fn(block_8_sc, block_bar_8);
            ranques_fn(block_9_sc, block_bar_9);
            ranques_fn(block_10_sc, block_bar_10);
        }
        if (quesions.Length == 0 && once == 0)
        {
            StartCoroutine(hold_ov());
            paper_white_1.SetActive(false);
            arrow_1.SetActive(false);
            once = 1;
        }
    }
    public void ranques_fn()
    {
        if (count > 0)
        {
            count--;
            StartCoroutine(hold());
        }
    }
    public void quit_4()
    {
        Application.Quit();
    }
    public void restart_4()
    {
        SceneManager.LoadScene(0);
    }
    private IEnumerator hold()
    {
        yield return new WaitForSeconds(0.2f);
        quesions[Random.Range(0, count)].transform.position = new Vector3(-3.98f, -2.8f, 0);
    }
    private IEnumerator hold_ov()
    {
        yield return new WaitForSeconds(0.1f);
        
        bg.SetActive(false);
        this.transform.position = new Vector3(0, 0, 0);
        block_1_sc = block_1.GetComponent<opt>().count;
        block_2_sc = block_2.GetComponent<opt>().count;
        block_3_sc = block_3.GetComponent<opt>().count;
        block_4_sc = block_4.GetComponent<opt>().count;
        block_5_sc = block_5.GetComponent<opt>().count;
        block_6_sc = block_6.GetComponent<opt>().count;
        block_7_sc = block_7.GetComponent<opt>().count;
        block_8_sc = block_8.GetComponent<opt>().count;
        block_9_sc = block_9.GetComponent<opt>().count;
        block_10_sc = block_10.GetComponent<opt>().count;

        if (block_1_sc == 0)
        {
            kindcheck++;
        }
        if (block_2_sc == 0)
        {
            kindcheck++;
        }
        if (block_3_sc == 0)
        {
            kindcheck++;
        }
        if (block_4_sc == 0)
        {
            kindcheck++;
        }
        if (block_5_sc == 0)
        {
            kindcheck++;
        }
        if (block_6_sc == 0)
        {
            kindcheck++;
        }
        if (block_7_sc == 0)
        {
            kindcheck++;
        }
        if (block_8_sc == 0)
        {
            kindcheck++;
        }
        if (block_9_sc == 0)
        {
            kindcheck++;
        }
        if (block_10_sc == 0)
        {
            kindcheck++;
        }

        if (kindcheck >= 7)
        {
            vic.GetComponent<RawImage>().texture = v3;
        }
        if (kindcheck > 2 && kindcheck < 7)
        {
            vic.GetComponent<RawImage>().texture = v2;
        }
        if (kindcheck <= 2)
        {
            vic.GetComponent<RawImage>().texture = v1;
        }

        go = true;

        yield return new WaitForSeconds(3f);
        this.GetComponent<AudioSource>().Play();
        vic.SetActive(true);
        restart.SetActive(true);
        quit.SetActive(true);
    }
    
    public void ranques_fn(int xx,GameObject yy)
    {
        if (xx == 1)
        {
            if (bar_nu_1 < 0.55f)
            {
                bar_nu_1 = bar_nu_1 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_1, 1, 1);
        }
        if (xx == 2)
        {
            if (bar_nu_2 < 1.12f)
            {
                bar_nu_2 = bar_nu_2 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_2, 1, 1);
        }
        if (xx == 3)
        {
            if (bar_nu_3 < 1.75f)
            {
                bar_nu_3 = bar_nu_3 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_3, 1, 1);
        }
        if (xx == 4)
        {
            if (bar_nu_4 < 2.32f)
            {
                bar_nu_4 = bar_nu_4 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_4, 1, 1);
        }
        if (xx == 5)
        {
            if (bar_nu_5 < 2.95f)
            {
                bar_nu_5 = bar_nu_5 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_5, 1, 1);
        }
        if (xx == 6)
        {
            if (bar_nu_6 < 3.5f)
            {
                bar_nu_6 = bar_nu_6 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_6, 1, 1);
        }
        if (xx == 7)
        {
            if (bar_nu_7 < 4.1f)
            {
                bar_nu_7 = bar_nu_7 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_7, 1, 1);
        }
        if (xx == 8)
        {
            if (bar_nu_8 < 4.69f)
            {
                bar_nu_8 = bar_nu_8 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_8, 1, 1);
        }
        if (xx == 9)
        {
            if (bar_nu_9 < 5.3f)
            {
                bar_nu_9 = bar_nu_9 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_9, 1, 1);
        }
        if (xx == 10)
        {
            if (bar_nu_10 < 5.88f)
            {
                bar_nu_10 = bar_nu_10 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_10, 1, 1);
        }
        if (xx == 11)
        {
            if (bar_nu_11 < 6.5f)
            {
                bar_nu_11 = bar_nu_11 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_11, 1, 1);
        }
        if (xx == 12)
        {
            if (bar_nu_12 < 7.06f)
            {
                bar_nu_12 = bar_nu_12 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_12, 1, 1);
        }
        if (xx == 13)
        {
            if (bar_nu_13 < 7.67f)
            {
                bar_nu_13 = bar_nu_13 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_13, 1, 1);
        }
        if (xx == 14)
        {
            if (bar_nu_14 < 8.25f)
            {
                bar_nu_14 = bar_nu_14 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_14, 1, 1);
        }
        if (xx == 15)
        {
            if (bar_nu_15 < 8.85f)
            {
                bar_nu_15 = bar_nu_15 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_15, 1, 1);
        }
        if (xx == 16)
        {
            if (bar_nu_16 < 9.44f)
            {
                bar_nu_16 = bar_nu_16 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_16, 1, 1);
        }
        if (xx == 17)
        {
            if (bar_nu_17 < 10.05f)
            {
                bar_nu_17 = bar_nu_17 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_17, 1, 1);
        }
        if (xx == 18)
        {
            if (bar_nu_18 < 10.62f)
            {
                bar_nu_18 = bar_nu_18 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_18, 1, 1);
        }
        if (xx == 19)
        {
            if (bar_nu_19 < 11.23f)
            {
                bar_nu_19 = bar_nu_19 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_19, 1, 1);
        }
        if (xx == 20)
        {
            if (bar_nu_20 < 11.79f)
            {
                bar_nu_20 = bar_nu_20 + 2f * Time.deltaTime;
            }
            yy.transform.localScale = new Vector3(bar_nu_20, 1, 1);
        }
    }
}
