﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GM_5 : MonoBehaviour
{
    float time;
    float time_1 = 60;
    public float ran_time;
    public float totaltime;
    public Text timetext;

    public GameObject goodblock_1;
    public GameObject goodblock_2;
    public GameObject goodblock_3;
    public GameObject goodblock_4;
    public GameObject goodblock_5;
    public GameObject goodblock_6;
    public GameObject goodblock_7;
    public GameObject badblock_1;
    public GameObject badblock_2;
    public GameObject badblock_3;
    public GameObject badblock_4;
    public GameObject badblock_5;
    public GameObject badblock_6;
    public GameObject badblock_7;
    GameObject block_cn;
    public bool gameover;
    public GameObject win_ob;
    public GameObject lose_ob;
    public GameObject restart_bt;
    public GameObject bat_1;
    public GameObject bat_2;

    // Start is called before the first frame update
    void Start()
    {
        rantime_fn();
    }

    // Update is called once per frame
    void Update()
    {
        totaltime = Mathf.Floor(time_1);
        timetext.text = totaltime.ToString();
        if (time_1 <= 0)
        {
            time_1 = 0;
            gameover = true;
        }

        if (gameover == false)
        {
            time_1 = time_1 - 1f * Time.deltaTime;
            time = time + 1f * Time.deltaTime;
            if (time >= ran_time)
            {
                ran_fn();
                ran__posfn(Random.Range(1, 5));
                Destroy(block_cn, 15f);
                time = 0;
                rantime_fn();
            }
        }

        if (gameover)
        {
            if (totaltime > 0)
            {
                win_ob.SetActive(true);
                restart_bt.SetActive(true);
            }
            if (totaltime <= 0)
            {
                lose_ob.SetActive(true);
                restart_bt.SetActive(true);
            }
        }
    }
    void rantime_fn()
    {
        ran_time = Random.Range(0.8f, 1.3f);
    }
    public void restart_fn()
    {
        SceneManager.LoadScene(0);
    }
    public void close_fn()
    {
        Application.Quit();
    }
    public int ranwhich;
    void ran_fn()
    {
        ranwhich = Random.Range(1,11);
        if (ranwhich < 4)
        {
            ran_badblock(Random.Range(1, 8));
        }
        if (ranwhich >= 4)
        {
            ran_goodblock(Random.Range(1, 8));
        }
    }
    void ran_goodblock(int xx)
    {
        if (xx == 1)
        {
            block_cn = Instantiate(goodblock_1);
            grachange_fn(block_cn);
        }
        if (xx == 2)
        {
            block_cn = Instantiate(goodblock_2);
            grachange_fn(block_cn);
        }
        if (xx == 3)
        {
            block_cn = Instantiate(goodblock_3);
            grachange_fn(block_cn);
        }
        if (xx == 4)
        {
            block_cn = Instantiate(goodblock_4);
            grachange_fn(block_cn);
        }
        if (xx == 5)
        {
            block_cn = Instantiate(goodblock_5);
            grachange_fn(block_cn);
        }
        if (xx == 6)
        {
            block_cn = Instantiate(goodblock_6);
            grachange_fn(block_cn);
        }
        if (xx == 7)
        {
            block_cn = Instantiate(goodblock_7);
            grachange_fn(block_cn);
        }
    }
    void ran_badblock(int xx)
    {
        if (xx == 1)
        {
            block_cn = Instantiate(badblock_1);
            grachange_fn(block_cn);
        }
        if (xx == 2)
        {
            block_cn = Instantiate(badblock_2);
            grachange_fn(block_cn);
        }
        if (xx == 3)
        {
            block_cn = Instantiate(badblock_3);
            grachange_fn(block_cn);
        }
        if (xx == 4)
        {
            block_cn = Instantiate(badblock_4);
            grachange_fn(block_cn);
        }
        if (xx == 5)
        {
            block_cn = Instantiate(badblock_5);
            grachange_fn(block_cn);
        }
        if (xx == 6)
        {
            block_cn = Instantiate(badblock_6);
            grachange_fn(block_cn);
        }
        if (xx == 7)
        {
            block_cn = Instantiate(badblock_7);
            grachange_fn(block_cn);
        }
    }
    void ran__posfn(int xx)
    {
        if (xx == 1)
        {
            block_cn.transform.localPosition = new Vector3(0, 26, 0);
        }
        if (xx == 2)
        {
            block_cn.transform.localPosition = new Vector3(-13, 26, 0);
        }
        if (xx == 3)
        {
            block_cn.transform.localPosition = new Vector3(-26, 26, 0);
        }
        if (xx == 4)
        {
            block_cn.transform.localPosition = new Vector3(13, 26, 0);
        }
        
    }
    void grachange_fn(GameObject xx)
    {
        if (time_1 < 50)
        {
            xx.GetComponent<Rigidbody2D>().gravityScale = 0.3f;
            bat_1.SetActive(true);
        }
        if (time_1 < 40)
        {
            xx.GetComponent<Rigidbody2D>().gravityScale = 0.5f;
        }
        if (time_1 < 30)
        {
            xx.GetComponent<Rigidbody2D>().gravityScale = 0.7f;
            bat_2.SetActive(true);
        }
        if (time_1 < 20)
        {
            xx.GetComponent<Rigidbody2D>().gravityScale = 0.9f;

        }
        if (time_1 < 10)
        {
            xx.GetComponent<Rigidbody2D>().gravityScale = 1f;
        }
    }
}
