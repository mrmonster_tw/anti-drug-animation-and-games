﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class move_5 : MonoBehaviour
{
    public SpriteRenderer tower_1;
    public SpriteRenderer tower_2;
    public SpriteRenderer tower_3;
    public SpriteRenderer tower_4;
    public SpriteRenderer tower_5;
    public SpriteRenderer tower_6;
    public SpriteRenderer tower_7;
    public SpriteRenderer tower_8;
    public SpriteRenderer tower_9;
    public SpriteRenderer tower_10;
    public Text percent;
    public GameObject gm;

    public AudioClip right;
    public AudioClip wrong;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (gm.GetComponent<GM_5>().gameover == false)
        {
            if (this.transform.position.x > -30.5f && Input.GetKey(KeyCode.LeftArrow))
            {
                this.transform.Translate(Vector3.left * 50f * Time.deltaTime);
            }

            if (this.transform.position.x < 16f && Input.GetKey(KeyCode.RightArrow))
            {
                this.transform.Translate(Vector3.right * 50f * Time.deltaTime);
            }
        }
       
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (gm.GetComponent<GM_5>().gameover == false)
        {
            Destroy(col.gameObject);
            if (col.tag == "good")
            {
                this.GetComponent<AudioSource>().clip = right;
                this.GetComponent<AudioSource>().Play();
                if (tower_1.sprite == null)
                {
                    tower_1.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "10";
                    return;
                }
                if (tower_2.sprite == null)
                {
                    tower_2.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "20";
                    return;
                }
                if (tower_3.sprite == null)
                {
                    tower_3.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "30";
                    return;
                }
                if (tower_4.sprite == null)
                {
                    tower_4.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "40";
                    return;
                }
                if (tower_5.sprite == null)
                {
                    tower_5.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "50";
                    return;
                }
                if (tower_6.sprite == null)
                {
                    tower_6.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "60";
                    return;
                }
                if (tower_7.sprite == null)
                {
                    tower_7.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "70";
                    return;
                }
                if (tower_8.sprite == null)
                {
                    tower_8.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "80";
                    return;
                }
                if (tower_9.sprite == null)
                {
                    tower_9.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "90";
                    return;
                }
                if (tower_10.sprite == null)
                {
                    tower_10.sprite = col.GetComponent<SpriteRenderer>().sprite;
                    percent.text = "100";
                    gm.GetComponent<GM_5>().gameover = true;
                    return;
                }
            }
            if (col.tag == "bad")
            {
                this.GetComponent<AudioSource>().clip = wrong;
                this.GetComponent<AudioSource>().Play();
                if (tower_9.sprite != null)
                {
                    tower_9.sprite = null;
                    percent.text = "90";
                    return;
                }
                if (tower_8.sprite != null)
                {
                    tower_8.sprite = null;
                    percent.text = "80";
                    return;
                }
                if (tower_7.sprite != null)
                {
                    tower_7.sprite = null;
                    percent.text = "70";
                    return;
                }
                if (tower_6.sprite != null)
                {
                    tower_6.sprite = null;
                    percent.text = "60";
                    return;
                }
                if (tower_5.sprite != null)
                {
                    tower_5.sprite = null;
                    percent.text = "50";
                    return;
                }
                if (tower_4.sprite != null)
                {
                    tower_4.sprite = null;
                    percent.text = "40";
                    return;
                }
                if (tower_3.sprite != null)
                {
                    tower_3.sprite = null;
                    percent.text = "30";
                    return;
                }
                if (tower_2.sprite != null)
                {
                    tower_2.sprite = null;
                    percent.text = "20";
                    return;
                }
                if (tower_1.sprite != null)
                {
                    tower_1.sprite = null;
                    percent.text = "10";
                    return;
                }
            }
        }
    }
}
